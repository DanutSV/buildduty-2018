from flask import Blueprint, render_template

bp = Blueprint(__name__, __name__, template_folder='templates')


@bp.route('/')
def homepage():
    return render_template("index.html")


@bp.route('/')
def danut():
    return render_template("danut.html")


@bp.route('/')
def bogdan():
    return render_template("bogdan.html")


@bp.route('/')
def cosmin():
    return render_template("cosmin.html")
